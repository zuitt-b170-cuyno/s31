// Responsible for performing CRUD operations
// Executes an operation once the route has been trigerred.

const Task = require('./../models/tasks')

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => result)
}

module.exports.createTask = (task) => {
    const newTask = new Task({ name: task.name })
    
    return newTask.save().then((createdTask, error) => {
        if (error) {
            console.log(error)
            return false
        }
        else 
            return createdTask
    })
}

module.exports.deleteTask = (id) => Task.findByIdAndRemove(id).then((removedTask, error) => {
    if (error) {
        console.log(error)
        return false
    }
    else 
        return removedTask
})   

module.exports.updateTask = (id, task) => {
	return Task.findById(id).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		} 
        else {
			result.name = task.name
			return result.save().then((updatedTask, error) => {
				if (error) {
					console.log(error)
					return false
				}
                else
					return updatedTask
			})
		}
	})
}

// S31 Activity
module.exports.getSpecificTask = id => Task.findById(id).then(task => task)

module.exports.updateTaskStatus = (id, status) => {
    return Task.findById(id).then((task, error) => {
        task.status = status
        return error ? false : task.save().then((updatedTask, error) => error ? false : updatedTask)
    })
}
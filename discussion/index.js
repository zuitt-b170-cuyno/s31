const express = require('express')
const mongoose = require('mongoose')
const TaskRouter = require('./routes/routes')
const app = express(), port = 3000

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.connect("mongodb+srv://jhnnycyn:dxtr008X@cluster0.z5eip.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

// Gives an app access to the TaskRoute
app.use("/tasks", TaskRouter)
app.listen(port, () => console.log(`Server is running at port:${port}`))
// Contains all the endpoints for the application

const express = require('express')
const TaskController = require('./../controllers/controllers')
// Creates a router instance that serves a middleware and routing system
// Allows access to HTTP methods that makes it easier to create routes
const router = express.Router()

router.get("/", (req, res) => {
    // TaskController - controller file that exists in the repository
    // getAllTasks - function that is encoded inside the TaskController file
    // The then(...) waits for getAllTasks to be executed
    TaskController.getAllTasks().then(result => res.send(result))
})

router.post("/", (req, res) => {
    TaskController.createTask(req.body).then(result => res.send(result))
})

// : (wildcard) - if tagged with parameter, the app will automatically look for the parameter that is in the URI

router.delete("/:id", (req, res) => {
    // URL parameter values are accessed through the request body's params. 
    // params matches the URL parameter endpoint
    TaskController.deleteTask(req.params.id).then(result => res.send(result))
})

router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})

// S31 Activity
router.get("/:id", (req, res) => TaskController.getSpecificTask(req.params.id).then(result => res.send(result)))
router.put("/:id/:status", (req, res) => {
    const { id, status } = req.params
    TaskController.updateTaskStatus(id, status).then(result => res.send(result))
})

module.exports = router